#!/usr/bin/env python
# **********************************************************************
#
# Copyright (c) 2003-2015 ZeroC, Inc. All rights reserved.
#
# **********************************************************************

import sys, traceback, time, Ice, IceStorm, getopt
import random

Ice.loadSlice('Clock.ice')
import Demo

# AUXILIARY FUNCTIONS
def strTimeProp(start, end, format, prop):
	stime = time.mktime(time.strptime(start, format))
	etime = time.mktime(time.strptime(end, format))

	ptime = stime + prop * (etime - stime)

	return time.strftime(format, time.localtime(ptime))

def randomDate(start, end, prop):
	return strTimeProp(start, end, '%m/%d/%Y', prop)

# OBJECT CLASS DEFINITION
class Runner:
	sensorId 	= 0
	totalKMs	= 0
	avgSpeed 	= 0
	age 		= 0
	height 		= 0
	weight		= 0
	hearthRate	= 0
	dateStamp	= ''


class Publisher(Ice.Application):
    def usage(self):
        print("Usage: " + self.appName() + " [--datagram|--twoway|--oneway] [topic]")

    def run(self, args):
        try:
            opts, args = getopt.getopt(args[1:], '', ['datagram', 'twoway', 'oneway'])
        except getopt.GetoptError:
            self.usage()
            return 1

        datagram = False
        twoway = False
        optsSet = 0
        topicName = "time"
        for o, a in opts:
            if o == "--datagram":
                datagram = True
                optsSet = optsSet + 1
            elif o == "--twoway":
                twoway = True
                optsSet = optsSet + 1
            elif o == "--oneway":
                optsSet = optsSet + 1

        if optsSet > 1:
            self.usage()
            return 1

        if len(args) > 0:
            topicName = args[0]

        manager = IceStorm.TopicManagerPrx.checkedCast(self.communicator().propertyToProxy('TopicManager.Proxy'))
        if not manager:
            print(args[0] + ": invalid proxy")
            return 1

        #
        # Retrieve the topic.
        #
        try:
            topic = manager.retrieve(topicName)
        except IceStorm.NoSuchTopic:
            try:
                topic = manager.create(topicName)
            except IceStorm.TopicExists:
                print(self.appName() + ": temporary error. try again")
                return 1

        #
        # Get the topic's publisher object, and create a Clock proxy with
        # the mode specified as an argument of this application.
        #
        publisher = topic.getPublisher();
        if datagram:
            publisher = publisher.ice_datagram();
        elif twoway:
            # Do nothing.
            pass
        else: # if(oneway)
            publisher = publisher.ice_oneway();
        clock = Demo.ClockPrx.uncheckedCast(publisher)

        print("publishing tick events. Press ^C to terminate the application.")
	runners = []
        try:
            while 1:
		runner = Runner()
		runner.sensorId 	= random.randint(1, 10)
		runner.totalKMs		= random.randint(1, 20)
		runner.avgSpeed 	= random.randint(1, 10)
		runner.age		= random.randint(18, 40)
		runner.height		= random.randint(160, 200)
		runner.weight		= random.randint(60, 90)
		runner.hearthRate	= random.randint(90, 140)
		runner.dateStamp	= randomDate("1/1/2016", "12/31/2016", random.random())
		runners.append(runner)
                clock.tick(str(runner.sensorId) + '^^' + str(runner.totalKMs) + '^^' + str(runner.avgSpeed) + '^^' + str(runner.age) + '^^' + str(runner.height) + '^^' + str(runner.weight) + '^^' + str(runner.hearthRate) + '^^' + str(runner.dateStamp))
                time.sleep(1)
        except IOError:
            # Ignore
            pass
        except Ice.CommunicatorDestroyedException:
            # Ignore
            pass
                
        return 0

app = Publisher()
sys.exit(app.main(sys.argv, "config.pub"))

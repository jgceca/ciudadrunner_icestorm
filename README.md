# **CIUDAD RUNNER** #

This repository contains the source code to execute the project made for the laboratory part of the subject IVI, teached in the Superior School of Computer Science in Ciudad Real.

### How to run the project? ###

Firts you have to download the code and unzip into your computer. Then, open a terminal inside that folder. There are a few steps to deploy the project.

### Deployment instructions ###

To run the demo, start the IceStorm service:

> icebox --Ice.Config=config.icebox

This configuration assumes there is a subdirectory named db in the current working directory.

In a separate window:

> python Subscriber.py

In another window:

> python Publisher.py

While the publisher continues to run, "tick" messages should be displayed in the subscriber window.

You will get a result that looks like this:

![Captura de pantalla de 2016-01-17 12-18-47.png](https://bitbucket.org/repo/BqKerG/images/4148852225-Captura%20de%20pantalla%20de%202016-01-17%2012-18-47.png)

### Who do I talk to? ###

* Repo owner

var Runners = require('./models/RunnersModel');

module.exports = function(app){
	// server routes ======================================
	/* handle things like api calls
	   authentication routes
	   sample api route
	*/

	app.get('/api/data', function(req, res){
		//use mongoose to get all runners from the mongolab db
		Runners.find({}, {'_id': 0, 'sensorId': 1, 'avgSpeed': 1, 'age': 1, 'height': 1, 'weight': 1, 'hearthRate': 1, 'totalKMs': 1, 'dateStamp': 1}, function(err, runnerDetails){
			// if there is an error retrieving, send the error.
				// nothing after res.send(err) will execute
		if(err) res.send(err);
		//console.log(runnerDetails);
		res.json(runnerDetails); // return the data in JSON
		});
	});

	app.get('*', function(req, res){
		res.sendFile('./public/index.html');
	});
}
